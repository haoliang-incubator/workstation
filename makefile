
build:
	make build-archlinux
	make build-workstation

build-archlinux:
	cd archlinux && make build

build-workstation:
	cd workstation && make build
