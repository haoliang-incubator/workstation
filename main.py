#!/usr/bin/env python3

import argparse
import ipaddress
import logging
import os
import shlex
import subprocess
from pathlib import Path
from typing import Union


# todo: need a more reliable way
def determine_host_ip():

    # default via 192.168.3.1 dev enp2s0 proto dhcp src 192.168.3.2 metric 1002
    cmd = shlex.split("ip route show scope global default")

    cp = subprocess.run(cmd, stdout=subprocess.PIPE, check=True)
    out = cp.stdout

    start = out.find(b" src ") + 4 + 1
    end = out.find(b" ", start)

    try:
        return ipaddress.IPv4Address(out[start:end].decode())
    except ipaddress.AddressValueError as e:
        raise RuntimeError("failed to find host ip") from e


class Facts:
    root = Path(__file__).resolve().parent
    var = root.joinpath("var")
    container = "workstation"
    image = "haoliang/workstation:latest"
    host_ip = None
    term = "xterm-256color"


def verbose_run(cmd_and_args: Union[list, str], check):
    if isinstance(cmd_and_args, str):
        cmd_and_args = shlex.split(cmd_and_args)

    logging.info(cmd_and_args)

    return subprocess.run(cmd_and_args, check=check)


def verbose_execvp(cmd_and_args: Union[list, str]):
    if isinstance(cmd_and_args, str):
        cmd_and_args = shlex.split(cmd_and_args)

    logging.info(cmd_and_args)

    os.execvp(cmd_and_args[0], cmd_and_args)


def start_container(host_ip):

    cmd = ["docker", "run"]

    parts = [
        # volumes
        f"""
        -v {Facts.var}/root:/root
        -v {Facts.var}/haoliang:/home/haoliang
        -v {Facts.var}/cache/pacman:/var/cache/pacman
        -v /srv/playground:/srv/playground
        -v /srv/pearl:/srv/pearl
        """,
        # resource limit
        "-m 8G --cpus 6",
        # see https://github.com/derekparker/delve/issues/515
        "--security-opt=seccomp:unconfined",
        # env
        f"""
        -e HOST_IP={host_ip}
        -e TERM={Facts.term}
        """,
        # misc
        f"""
        -p 127.0.0.1:8000:8000
        --net=hub
        --name {Facts.container}
        -w /home/haoliang
        """,
    ]

    for part in parts:
        cmd.extend(shlex.split(part))

    cmd.extend(("-d", Facts.image))

    verbose_run(cmd, check=True)


def parse_args(args=None):
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verbose", action="count", default=0)
    subcmd = parser.add_subparsers(dest="subcmd", required=True)

    start = subcmd.add_parser("start")
    start.add_argument("-i", "--host-ip", type=str, required=True)
    restart = subcmd.add_parser("restart")
    restart.add_argument("-i", "--host-ip", type=str, required=True)

    subcmd.add_parser("stop")

    shell = subcmd.add_parser("shell", help="open a interacitve shell in container")
    shell.add_argument("shell", type=str, nargs="?", default="zsh")

    command = subcmd.add_parser("command", help="oneshot command runs in container")
    command.add_argument("command", type=str)

    return parser.parse_args(args)


def main_with_args(args):
    def stop_container():
        verbose_run(f"docker stop {Facts.container}", check=False)
        verbose_run(f"docker rm {Facts.container}", check=True)

    def restart_container(host_ip):
        stop_container()
        start_container(host_ip)

    def shell_in_container(shell: str):
        verbose_execvp(f"docker exec -it {Facts.container} {shell} -i")

    def command_in_container(command: str):
        verbose_execvp(f"docker exec -t {Facts.container} {command}")

    match args.subcmd:
        case "start":
            start_container(args.host_ip)
        case "stop":
            stop_container()
        case "restart":
            restart_container(args.host_ip)
        case "shell":
            shell_in_container(args.shell)
        case "command":
            command_in_container(args.command)
        case _ as subcmd:
            raise SystemExit(f"unknown subcmd {subcmd}")


if __name__ == "__main__":

    args = parse_args()
    level = ("ERROR", "INFO", "DEBUG")[max(2, args.verbose)]

    logging.basicConfig(
        level=level,
        style="{",
        datefmt="%Y-%m-%d %H:%M:%S",
        format="{asctime} {message}",
    )

    main_with_args(args)
