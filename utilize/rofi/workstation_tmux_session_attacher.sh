#!/usr/bin/env bash

set -e

command_in_container() {
    python3 $UTILIZE_BIN command "$*"
}

list_sessions() {
    command_in_container tmux list-session -F "#{session_name}" || {
        notify "failed to list session"
        return 1
    }
}

attach_to_session() {
    local session=${1:?requires session name}

    local mutex_option=""
    if [ $MUTEX -ne 0 ]; then
        mutex_option="-d"
    fi

    coproc {
        # todo urxvtc -e
        command_in_container tmux attach -t "$session" $mutex_option
    }

    local in=${COPROC[1]}
    exec {in}>&-
}

attach_to_new_session() {
    local session="${1:?requires session param}"
    local _workdir="${2:?requires workdir param}"

    local workdir
    workdir=$(realpath "${_workdir/#\~/$HOME}") || {
        notify "invalid workdir: ${_workdir}"
        return 1
    }

    command_in_container tmux new-session -d -c "$workdir" -s $session || {
        notify "failed to create session using name '$session' and workdir '$workdir'"
        return 1
    }

    attach_to_session $session
}

main() {

    # NB: rofi always gives one argument
    # shellcheck disable=SC2116
    set -- $(echo "$@")

    case $# in
        0)
            list_sessions
            ;;
        2)
            attach_to_new_session "$@"
            ;;
        1|*)
            attach_to_session "$*"
            ;;
    esac

}

ROOT=$(dirname $(realpath $0))
UTILIZE_BIN=$(realpath $ROOT/../__main__.py)
MUTEX=${MUTEX:-1}

main "$@"
