
plugins for rofi:
---

* workstation\_tmux\_sessions: you can select all tmux session existed in workstation container from rofi, and attach the session in a new terminal window

usage
---

rofi -show workstation.tmux.sessions -modi workstation.tmux.sessions:/this/workstation\_tmux\_sessions.sh

