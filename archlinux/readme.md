build
---

* audit upstream.pacman.conf, and update docker/config/pacman.conf
* docker with buildkit
* http\_proxy for auracle installing

includes
---

* archlinux:latest
* group: base, base-devel
* a normal user named haoliang with password xx
* auracle

enable hooks for git
---

* ln -s $(pwd)/hook/pre-commit $(pwd).git/hooks/

