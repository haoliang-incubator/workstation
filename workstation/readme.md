
特别设定
---

linux distro: archlinux

volumes:
* /root
* /home/haoliang
* /srv/{playground,pearl}

default user:
* uid=1000
* name=haoliang

env:
* HOST\_IP

resource limit:
* cpu 80%
* memory 80%

network: hub


setup for a user
---

ENV SHELL
* since SHELL is set by logind, we need set it manually
* http://man.openbsd.org/login.1
* why set SHELL: tmux uses it to spawn pty

~/.profile
* since we `docker exec $SHELL -i`, interactive shell, $SHELL will never get
  chance to source ~/.profile, we need source it manually


FAQ
----

how to use xdebug
    * xdebug reads environment variable called `XDEBUG_CONFIG` and overwrite the config file configurations
    * but [few option](https://xdebug.org/docs/remote) can use:
        * idekey (in my tests, this not works)
        * remote_host
        * remote_port
        * remote_mode
        * remote_handler

`ctrl-p twice`
    * https://stackoverflow.com/questions/20828657/docker-change-ctrlp-to-something-else

